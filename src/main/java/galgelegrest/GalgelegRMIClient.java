/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgelegrest;

import brugerautorisation.RMI.IBrugerAutorisationRMI;
import galgeleg.interfaces.RMI.IGame;
import galgeleg.interfaces.RMI.IGameID;
import galgeleg.interfaces.RMI.IHighscore;


import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 *
 * @author Tobias
 */
public class GalgelegRMIClient {
    
    private static String registry = "rmi://localhost:2002/";

    public static String getRegistry() {
        return registry;
    }

    public static void setRegistry(String registry) {
        GalgelegRMIClient.registry = registry;
    }

    public static IGame getIGame() throws NotBoundException, MalformedURLException, RemoteException {
        return  (IGame) Naming.lookup("rmi://localhost:2002/Game");

        //return (IGame) Naming.lookup(registry+"Game");
    }

    public static IGameID getIGameID() throws NotBoundException, MalformedURLException, RemoteException {
        return (IGameID) Naming.lookup(registry+"GameID");
    }

    public static IHighscore getIHighscore() throws NotBoundException, MalformedURLException, RemoteException {
        return (IHighscore) Naming.lookup(registry+"Highscore");
    }
    
    public static boolean validateLogin(String brugernavn, String adgangskode) throws NotBoundException, MalformedURLException, RemoteException, Exception{
            IBrugerAutorisationRMI ba = (IBrugerAutorisationRMI) Naming.lookup(registry+"galgelogik");
            if(ba.login(brugernavn, adgangskode) != null){
                return true;
            }
            return false;
    }
    
    
    
}
