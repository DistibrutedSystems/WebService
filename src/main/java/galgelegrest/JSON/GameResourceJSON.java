/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgelegrest.JSON;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 * REST Web Service
 *
 * @author Tobias
 */
@Path("galgelegrest/JSON/Game")
public class GameResourceJSON {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GameResourceJSON
     */
    public GameResourceJSON() {
    }

    /**
     * Retrieves representation of an instance of gr02.distribuerede_systemer.galgelegrest.GameResourceJSON
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of GameResourceJSON
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
