/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgelegrest.utils;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

/**
 *
 * @author Tobias
 */
public class MustacheCompiler {
    
    public static String compile(String template, Map<String, Object> mustacheData) throws IOException{
        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache m = mf.compile(template);
        //render template with data
        StringWriter writer = new StringWriter();
        m.execute(writer, mustacheData).flush();
        return writer.toString();
    }
}
