/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgelegrest.HTML;

import brugerautorisation.RMI.IBrugerAutorisationRMI;
import galgelegrest.GalgelegRMIClient;
import galgelegrest.utils.MustacheCompiler;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Tobias
 */
@Path("HTML/Login")
public class LoginResource {

    private String template = "loginpage.html";
    private String response = "loginresponse.txt";
    
    @Context
    private HttpServletRequest context;

    /**
     * Creates a new instance of LoginResource
     */
    public LoginResource() {
    }

    /**
     * Retrieves representation of an instance of galgelegrest.HTML.LoginResource
     * @return an instance of javax.ws.rs.core.Response
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response getHtml() throws IOException {
        Map<String, Object> mustacheData = new HashMap<String, Object>();
        return Response.ok(MustacheCompiler.compile(template, mustacheData), MediaType.TEXT_HTML).build();
    }

    /**
     * PUT method for updating or creating an instance of LoginResource
     * @param content representation for the resource
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public Response putHtml(@FormParam("username") String user, @FormParam("password") String pass) throws IOException {
        try {
            if(user != null && pass != null && GalgelegRMIClient.validateLogin(user, pass)){
                
                    NewCookie username = new NewCookie("username", user);
                    NewCookie password = new NewCookie("password", pass);

                    Map<String, Object> mustacheData = new HashMap<String, Object>();
                    mustacheData.put("message", "Velkommen ");
                    mustacheData.put("id", user);
                    mustacheData.put("link", user);
                    return Response.ok(MustacheCompiler.compile(response, mustacheData)).cookie(username, password).build();
                
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(LoginResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(LoginResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(LoginResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return getHtml();
    }
}
