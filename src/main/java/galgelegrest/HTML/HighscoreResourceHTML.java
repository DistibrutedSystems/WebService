/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgelegrest.HTML;

import galgelegrest.GalgelegRMIClient;
import galgeleg.interfaces.RMI.IHighscore;
import galgelegrest.utils.MustacheCompiler;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Tobias
 */
@Path("HTML/Highscore")
public class HighscoreResourceHTML {

    private String template = "test.html";
    
    @Context
    private HttpServletRequest context;

    /**
     * Creates a new instance of HighscoreResourceHTML
     */
    public HighscoreResourceHTML() {
    }

    /**
     * Retrieves representation of an instance of gr02.distribuerede_systemer.galgelegrest.HighscoreResourceHTML
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response getHtml() throws IOException {
        List<String> highscores = new ArrayList<>();
        highscores.add("Many");
        highscores.add("Example");
        highscores.add("Much");
        highscores.add("Wow");
        
        Map<String, Object> mustacheData = new HashMap<String, Object>();
        mustacheData.put("data", highscores);
        
        try {
            IHighscore ih = GalgelegRMIClient.getIHighscore();
            mustacheData.put("data", ih.get());
        } catch (NotBoundException ex) {
            Logger.getLogger(HighscoreResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(HighscoreResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(HighscoreResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(MustacheCompiler.compile(template, mustacheData), MediaType.TEXT_HTML).build();
    }

    /**
     * PUT method for updating or creating an instance of HighscoreResourceHTML
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.TEXT_HTML)
    public Response putHtml(String content) {
        
        //Eksempel på put til senere
        try {
            return getHtml();
        } catch (IOException ex) {
            Logger.getLogger(HighscoreResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
}
