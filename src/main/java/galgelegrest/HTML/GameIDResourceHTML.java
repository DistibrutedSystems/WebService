/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgelegrest.HTML;

import galgeleg.interfaces.RMI.IGalgelogikRMI;
import galgelegrest.GalgelegRMIClient;
import galgeleg.interfaces.RMI.IGameID;
import galgelegrest.utils.MustacheCompiler;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Tobias
 */
@Path("HTML/Game/{id}")
public class GameIDResourceHTML {

    private String template = "gameidpage.html";
    
    @Context
    private HttpServletRequest context;

    /**
     * Creates a new instance of GameIDResource
     */
    public GameIDResourceHTML() {
    }

    /**
     * Retrieves representation of an instance of gr02.distribuerede_systemer.galgelegrest.GameIDResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response getHtml(@PathParam("id") String id) throws IOException {
        //Set some data
        Map<String, Object> mustacheData = new HashMap<String, Object>();
        mustacheData.put("data", "Specific game data with id="+id+" not found. Server might be down.");
        //mustacheData.put("message", context.getSession());
        try {
            IGameID ig = GalgelegRMIClient.getIGameID();
            mustacheData.put("data", ig.get(id).getStatusBesked());
        } catch (NotBoundException ex) {
            Logger.getLogger(galgelegrest.HTML.GameResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(galgelegrest.HTML.GameResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(galgelegrest.HTML.GameResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        //Insert data into file and return it
        return Response.ok(MustacheCompiler.compile(template, mustacheData), MediaType.TEXT_HTML).build();
    }

    /**
     * PUT method for updating or creating an instance of GameIDResource
     * @param letter
     * @param content representation for the resource
     * @return Response
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response postHtml(@CookieParam("username") Cookie user, @CookieParam("password") Cookie pass,
            @FormParam("letter") String letter, @PathParam("id") String id) throws IOException {
        if(user != null && pass != null){
            //We take the user data
            Map<String, String> data = new HashMap<>();
            data.put("username", user.getValue());
            data.put("password", pass.getValue());
            if(letter != null && id != null){
                if(letter.length() == 1){
                    try {
                        IGameID ig = GalgelegRMIClient.getIGameID();
                        IGalgelogikRMI logik = ig.get(id);
                        logik.gætBogstav(letter);
                        ig.put(id,logik, data);
                    } catch (NotBoundException ex) {
                        Logger.getLogger(GameIDResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (MalformedURLException ex) {
                        Logger.getLogger(GameIDResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (RemoteException ex) {
                        Logger.getLogger(GameIDResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            return getHtml(id);
        }
        try {
            //If we got null as a response, or the user was not logged in, that means the login data is invalid. The user is redirected to the login page
            java.net.URI location = new java.net.URI("../REST/HTML/Login");
            return Response.temporaryRedirect(location).build();
        } catch (URISyntaxException ex) {
            Logger.getLogger(GameResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().entity("ERROR").build();
        }      
    }
}
