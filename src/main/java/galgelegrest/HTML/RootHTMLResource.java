/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgelegrest.HTML;

import galgelegrest.utils.MustacheCompiler;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Tobias
 */
@Path("HTML")
public class RootHTMLResource {

    private String template = "HTMLpage.html";
    
    @Context
    private HttpServletRequest context;

    /**
     * Creates a new instance of RootHTMLResource
     */
    public RootHTMLResource() {
    }

    /**
     * Retrieves representation of an instance of gr02.distribuerede_systemer.RootHTMLResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response getHtml() throws IOException {
        Map<String, Object> mustacheData = new HashMap<String, Object>();
        return Response.ok(MustacheCompiler.compile(template, mustacheData), MediaType.TEXT_HTML).build();
    }
}
