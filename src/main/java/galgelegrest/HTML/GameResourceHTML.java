/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgelegrest.HTML;

import galgeleg.interfaces.RMI.IGalgelogikRMI;
import galgelegrest.GalgelegRMIClient;
import galgeleg.interfaces.RMI.IGame;
import galgelegrest.utils.MustacheCompiler;


import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;


/**
 * REST Web Service
 *
 * @author Tobias
 */
@Path("HTML/Game")
public class GameResourceHTML {
    
    private String template = "gamepage.html";

    @Context
    private HttpServletRequest context;

    /**
     * Creates a new instance of GameResource
     */
    public GameResourceHTML() {
    }

    /**
     * Retrieves representation of an instance of gr02.distribuerede_systemer.galgelegrest.GameResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response getHtml() throws IOException {
        //Set some data
        Map<String, Object> mustacheData = new HashMap<String, Object>();
        mustacheData.put("data", "Game data not found.");
        mustacheData.put("message", context.getSession());
        try {
            IGame igame = GalgelegRMIClient.getIGame();
            Map<String, IGalgelogikRMI> map = igame.get();
            StringBuilder sb = new StringBuilder();
            for(String s : map.keySet()){
                //sb.append("<a href=\"/" + s + "\">"+ s +"</a><br>");
                sb.append(s +"\n");
            }
            mustacheData.put("data", sb.toString());
        } catch (NotBoundException ex) {
            Logger.getLogger(GameResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(GameResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(GameResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GameResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        //Insert data into file and return it
        return Response.ok(MustacheCompiler.compile(template, mustacheData), MediaType.TEXT_HTML).build();
        
    }

    /**
     * PUT method for updating or creating an instance of GameResource
     * @param content representation for the resource
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response putHtml(@CookieParam("username") Cookie user, @CookieParam("password") Cookie pass) {
        
        //System.out.println("HHHHHHHHHHHHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa");
        
        //If the user is logged in
        if(user != null && pass != null){
            try {
                //We take the user data
                Map<String, String> data = new HashMap<>();
                data.put("username", user.getValue());
                data.put("password", pass.getValue());
                //And use it to add a new game to the list through the put method of the IGame interface
                IGame igame = GalgelegRMIClient.getIGame();
                String id = igame.put(data);
                //If a string other than null is returned, we know that the game has been successfully added, and can be found at the id given back by the method
                if(id != null){
                    //We redirect the page to the new game
                    java.net.URI location = new java.net.URI("../REST/HTML/Game/"+id);
                    return Response.temporaryRedirect(location).build();
                }        
            } catch (NotBoundException ex) {
                Logger.getLogger(GameResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MalformedURLException ex) {
                Logger.getLogger(GameResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
            } catch (RemoteException ex) {
                Logger.getLogger(GameResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(GameResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
            }     
        } 
        try {
            //If we got null as a response, or the user was not logged in, that means the login data is invalid. The user is redirected to the login page
            java.net.URI location = new java.net.URI("../REST/HTML/Login");
            return Response.temporaryRedirect(location).build();
        } catch (URISyntaxException ex) {
            Logger.getLogger(GameResourceHTML.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().entity("ERROR").build();
        }
    }
}
