package galgeleg.interfaces.RMI;

import java.util.HashMap;
import java.util.Map;

public interface IGame extends java.rmi.Remote {
    HashMap<String, IGalgelogikRMI> get() throws Exception;

    String put(Map<String, String> login) throws Exception;
}