package galgeleg.interfaces.RMI;

import java.util.Map;

public interface IGameID extends java.rmi.Remote {
    IGalgelogikRMI get(String id) throws java.rmi.RemoteException;

    void put(String id, IGalgelogikRMI game, Map<String, String> login) throws java.rmi.RemoteException;

    void delete(String id, Map<String, String> login) throws java.rmi.RemoteException;
}
